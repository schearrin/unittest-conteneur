package test;

import testEtat.*;
import junit.framework.*;

public class TestInit extends TestCase {

	private Conteneur C;
	private int CAPACITE = 5;

	// Objectif de test : creation d'un conteneur de capacite > 1
	// Resultat attendu : conteneur vide cree de la capacite passee en argument
	public void testCapaciteSup1() {
		try {
			C = new Conteneur(CAPACITE);

			assertNotNull(C);
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
			assertTrue(C.estVide());
		}
		catch (Exception e) {
			fail(); 	// on force le test a echouer si une exception est levee
		}
	}

	// Objectif de test : creation d'un conteneur de capacite egal a 1
	// Resultat attendu : impossible, levee d'une exception de type ErreurConteneur
	public void testCapaciteEqual1(){
		try{
			C = new Conteneur(1);
			
			assertNotNull(C);
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), 1);
			assertTrue(C.estVide());
			fail("fail, capacite egal a 1");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail("fail, si levee d'une exception autre que ErreurConteneur");
		}
	}
	
	// Objectif de test : creation d'un conteneur de capacite egal a 0
	// Resultat attendu : impossible, levee d'une exception de type ErreurConteneur
	public void testCapaciteEqual0(){
		try{
			C = new Conteneur(0);
			
			assertNotNull(C);
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), 0);
			assertTrue(C.estVide());
			fail("fail, capacite egal a 0");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail("fail, si levee d'une exception autre que ErreurConteneur");
		}
	}
	
	// Objectif de test : creation d'un conteneur de capacite negative
	// Resultat attendu : impossible, levee d'une exception de type ErreurConteneur
	public void testCapaciteInf0(){
		try{
			C = new Conteneur(-1);
						
			assertNotNull(C);
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), -1);
			assertTrue(C.estVide());
			fail("fail, capacite negative");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail("fail, si levee d'une exception autre que ErreurConteneur");
		}
	}
}
