package test;
import rarray.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestRarrayInit {

	Rarray A;

	// objectif : tester la creation d'un Rarray de capacite strictement positive
	// resultat attendu : un Rarray de taille 0 est cree
	@Test
	public void initTaillePos() {
		assertNull(A);
		
		try {
			A = new Rarray(10);
		} catch (Exception e) {
			fail();
		}
		assertNotNull(A);
		assertEquals(0,A.size());
	}
	
	// objectif : tester la creation d'un Rarray de capacite strictement négative
	// resultat attendu : pas de Rarray cree
	@Test
	public void initTailleNeg() {
		assertNull(A);
		
		try{
			A = new Rarray(-10);
			fail();
		} catch(Exception e){

		}
		assertNull(A);
	}
	
	// objectif : tester la creation d'un Rarray de capacite nulle
	// resultat attendu : pas de Rarray cree
	@Test
	public void initTaille0() {
		assertNull(A);
		
		try{
			A = new Rarray(0);
			fail();
		} catch(Exception e){
			
		}
		assertNull(A);
	}

}

