package test;

import testEtat.*;
import junit.framework.*;

public class TestVide extends TestCase {

	private Conteneur C;
	private int CAPACITE = 10;

	// Creation d'un conteneur vide	
	public void setUp() {
		try {
			C = new Conteneur(10);
		} catch (Exception e) {
			fail();
		}
	}
	
	// Objectif de test : test est vide dans un conteneur vide
	// Resultat attendu : le contenu est vide
	public void testEstVideVide(){
		try{			
			assertTrue(C.estVide());
			
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : retrouver la valeur associee a une clef absente
	// Resultat attendu : impossible, exception erreur conteneur signalee
	public void testValeurClefAbsenteVide(){
		try{
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();
			
			C.valeur(K);
			
			fail("fail, impossible de trouver la valeur");
		}
		catch(ErreurConteneur e){

		}
		catch(Exception e){
			fail("fail, si levee d'une autre exception que erreur conteneur");
		}
	}
	
	// Objectif de test : ajout d'un nouveau couple clef-valeur a un conteneur vide
	// Resultat attendu : possible
	public void testAjouterNonPresentVide(){
		try{
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();
			Object V = new Object();
			
			C.ajouter(K, V);
			
			assertTrue(C.present(K));
			assertEquals(C.valeur(K),V);
			assertEquals(C.taille(), 1);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail();
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : retirer une clef absente d'un conteneur vide
	// Resultat attendu : rien ne se passe donc pas de levee d'exception
	public void testRetirerAbsentVide(){
		try{
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();

			C.retirer(K);
			
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}		
	}
	
	// Objectif de test : remise a zero d'un conteneur vide
	// Resultat attendu : remise a zero impossible, levee de l'exception ErreurConteneur
	public void testRazVide() {
		try {
			C.raz();
			fail();
			// on force le test a echouer si aucune exception n'est levee
		} catch (ErreurConteneur e) {
			// si une exception de type ErreurConteneur est levee, le test reussit
			// on verifie que le conteneur n'a pas ete modifie
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
			assertTrue(C.estVide());
		} catch (Exception e) {
			fail();
			// si une exception d'un autre type est levee, le test echoue 
		}
	}
	
	// Objectif de test : redimensionner un conteneur vide en fixant une capacite plus grande
	// Resultat attendu : impossible pour un conteneur vide, levee exception erreur conteneur
	public void testRedimPlusGrandeVide(){
		try{
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);

			C.redimensionner(20);

			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), 20);
			fail("fail, impossible de redimensionner un conteneur vide");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : redimensionner un conteneur vide en fixant une capacite plus petite
	// Resultat attendu : impossible, levee de l'exception erreur conteneur
	public void testRedimPlusPetiteVide(){
		try{
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
			
			C.redimensionner(3);
			
			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), 3);
			fail("fail, impossible de redimensionner");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail();
		}
	}	
}
