package test;
import rarray.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestRarrayNonVide {
	
	Rarray A;
	Object a,b,c,d;
	
	@Before
	public void creerRarrayNonVide() throws Exception {

		try {
			A = new Rarray(10);
			a = new Object();
			b = new Object();
			c = new Object();
			d = new Object();
			A.add(a);
			A.add(b);
			A.add(c);
			A.add(a);
			A.add(a);
			A.add(c);
		} catch (RarrayError e) {
			fail();
		}
	}
	
	// objectif : test prambule d'un Rarray non vide
	// resultat attendu : le Rarray est bien non vide et tout les objets sont bien present
	@Test
	public void testPreambuleNonVide(){
		assertEquals(6,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertEquals(3,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
	}
	
	// objectif : tester la suppression d'un objet present plusieurs fois
	// resultat attendu : une seule occurrence de l'objet est supprimée
	@Test
	public void removeMult() {
		Boolean r = A.remove(a);

		assertTrue(r);
		assertEquals(5,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertEquals(2,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
	}
	
	// objectif : tester la suppression de tous les meme elements d'un RarrayNonVide
	// resultat attendu : toutes les occurences de l'objet sont supprimees
	@Test
	public void removeAllMult() {
		Boolean r = A.removeAll(a);
		
		assertTrue(r);
		assertEquals(3,A.size());
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertFalse(A.contains(a));
		assertEquals(0,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
	}
	
	// objectif : tester la remise a zero d'un RarrayNonVide
	// resultat attendu : tout les objets du Rarray sont supprimes
	@Test
	public void testclearAll() {
		A.clear();
		
		assertEquals(0,A.size());
		assertFalse(A.contains(a));
		assertFalse(A.contains(b));
		assertFalse(A.contains(c));
		assertEquals(0,A.nbOcc(a));
		assertEquals(0,A.nbOcc(b));
		assertEquals(0,A.nbOcc(c));
	}
	
	// objectif : tester la suppression non existant d'un element dans un RarrayNonVide
	// resultat attendu : la methode remove renvoie Faux
	@Test
	public void testRemoveNotExist() {
		Boolean r = A.remove(d);
		
		assertFalse(r);
		assertEquals(6,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertEquals(3,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
	}
	
	// objectif : tester l'ajout d'un element existant non vide
	// resultat attendu : l'objet existant est ajoute
	@Test
	public void testAddExist() {
		A.add(a);
		
		assertEquals(7,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertEquals(4,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
	}
	
	
	// objectif : tester l'ajout d'un element non existant dans un Array non vide
	// resultat attendu : l'objet non existant est ajoute
	@Test
	public void testAddNotExist() {
		A.add(d);
		
		assertEquals(7,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertTrue(A.contains(d));
		assertEquals(3,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
		assertEquals(1,A.nbOcc(d));
	}
	
	// objectif : tester l'ajout et la suppression d'un element existant dans un Array non vide
	// resultat attendu : rien a change
	@Test
	public void testAddRemoveExist() {
		A.add(a);
		
		assertEquals(7,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertEquals(4,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
	
		A.remove(a);
		
		assertEquals(6,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertEquals(3,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
	}
	
	// objectif : tester l'ajout et la suppression d'un element non existant dans un Array non vide
	// resultat attendu : rien a change
	@Test
	public void testAddRemoveNotExist() {
		A.add(d);
		
		assertEquals(7,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertTrue(A.contains(d));
		assertEquals(3,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
		assertEquals(1,A.nbOcc(d));
	
		A.remove(d);
		
		assertEquals(6,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertFalse(A.contains(d));
		assertEquals(3,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
		assertEquals(0,A.nbOcc(d));
	}
	
	// objectif : tester l'ajout des objets present en depassant la capacite de A
	// resultat attendu : ajout possible et capacite augmente
	@Test
	public void testAddDepasseExist() {
		try{
			A.add(a);
			A.add(a);
			A.add(a);
			A.add(a);
			A.add(a);
		} catch(Exception e){
			fail("Fail car la capacite est augmente si besoin");
		}
		assertEquals(11,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertFalse(A.contains(d));
		assertEquals(8,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
		assertEquals(0,A.nbOcc(d));
	}
	
	// objectif : tester l'ajout des objets non present en depassant la capacite de A
	// resultat attendu : ajout possible et capacite augmente
	@Test
	public void testAddDepasseNotExist() {
		try{
			A.add(d);
			A.add(d);
			A.add(d);
			A.add(d);
			A.add(d);
		} catch(Exception e){
			fail("Fail car la capacite est augmente si besoin");
		}
		assertEquals(11,A.size());
		assertTrue(A.contains(a));
		assertTrue(A.contains(b));
		assertTrue(A.contains(c));
		assertTrue(A.contains(d));
		assertEquals(3,A.nbOcc(a));
		assertEquals(1,A.nbOcc(b));
		assertEquals(2,A.nbOcc(c));
		assertEquals(5,A.nbOcc(d));
	}

	
	//
	
	// objectif : test si l'objet dont on cherche l'indice est present
	// resultat attendu : indice non present, leve l'exception ObjectNotFound
	@Test
	public void testIndexNotExist() {
		try{
			A.index(d);
			fail();
		} catch(ObjectNotFound e){
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
	}

	// objectif : test si l'objet dont on cherche l'indice est present
	// resultat attendu : indice present
	@Test
	public void testIndexExist() {
		try{
			A.index(b);
			assertEquals(1,A.index(b));
			A.index(a);
			assertEquals(0,A.index(a));
		} catch(ObjectNotFound e){
			fail();
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
	}
	
	// objectif : recherche l'indice non present dans tableau
	// resultat attendu : leve l'exception OutOfRarray
	@Test
	public void testGetNotExist() {
		try{
			A.get(11);
			fail();
		} catch(OutOfRarray e){
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
	}
	
	// objectif : recherche l'indice present d'un element present dans tableau
	// resultat attendu : fonctionne, indice de l'objet present, ne leve pas l'exception OutOfRarray
	@Test
	public void testGetExist() {
		try{
			A.get(1);
			assertEquals(b,A.get(1));
		} catch(OutOfRarray e){
			fail();
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
	}
	
	// objectif : recherche l'indice present d'un element non present dans tableau
	// resultat attendu : leve l'exception OutOfRarray
	@Test
	public void testGetExistNotObj() {
		try{
			A.get(7);
			fail();
		} catch(OutOfRarray e){
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
	}

	// objectif : supprime l'objet d'un indice present
	// resultat attendu : fonctionne
	@Test
	public void testRemoveIndPresent() {
		try{
			A.removeInd(0);
		} catch(OutOfRarray e){
			fail();
			assertEquals(5,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(2,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
	}
	
	// objectif : supprime l'objet d'un indice hors Rarray non present
	// resultat attendu : leve l'exception OutOfRarray
	@Test
	public void testRemoveIndNonPresentCD() {
		try{
			A.removeInd(11);
			fail();
		} catch(OutOfRarray e){
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
	}
		
	// objectif : supprime l'objet d'un indice non present
	// resultat attendu : leve l'exception OutOfRarray
		@Test
		public void testRemoveIndNonPresent() {
			try{
				A.removeInd(7);
				fail();
			} catch(OutOfRarray e){

			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
	}

		// objectif : ajout et supprime l'objet � partir d'un indice
		// resultat attendu : fonctionne
		@Test
		public void testAddRemoveInd() {
			try{
				A.add(d);
				assertEquals(d,A.get(6));
				A.removeInd(6);
			} catch(OutOfRarray e){
				fail();
			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertFalse(A.contains(d));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
			assertEquals(0,A.nbOcc(d));
		}
		
		// objectif : ajout d'un objet d�j� pr�sent et supprime l'objet � partir d'un indice
		// resultat attendu : fonctionne
		@Test
		public void testAddRemoveIndP() {
			try{
				A.add(a);
				assertEquals(a,A.get(6));
				A.removeInd(6);
			} catch(OutOfRarray e){
				fail();
			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertFalse(A.contains(d));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
			assertEquals(0,A.nbOcc(d));
		}
		
		// objectif : remplace un objet � un autre � indice pr�sent
		// resultat attendu : fonctionne
		@Test
		public void testReplaceIndP() {
			try{
				A.replace(1, d);
				assertEquals(d,A.get(1));
			} catch(OutOfRarray e){
				fail();
			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertFalse(A.contains(b));
			assertTrue(A.contains(c));
			assertTrue(A.contains(d));
			assertEquals(3,A.nbOcc(a));
			assertEquals(0,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
			assertEquals(1,A.nbOcc(d));
		}
		
		// objectif : remplace un objet � un autre � indice par le m�me objet
		// resultat attendu : fonctionne, reste pareil
		@Test
		public void testReplaceIndP2() {
			try{
				A.replace(1, b);
				assertEquals(b,A.get(1));
			} catch(OutOfRarray e){
				fail();
			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertFalse(A.contains(d));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
			assertEquals(0,A.nbOcc(d));
		}
		
		// objectif : remplace un objet � un autre � indice par un objet qui existe d�j�
		// resultat attendu : fonctionne
		@Test
		public void testReplaceIndP3() {
			try{
				A.replace(1, a);
				assertEquals(a,A.get(1));
			} catch(OutOfRarray e){
				fail();
			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertFalse(A.contains(b));
			assertTrue(A.contains(c));
			assertEquals(4,A.nbOcc(a));
			assertEquals(0,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
		}
		
		// objectif : remplace un objet � un autre � indice non pr�sent
		// resultat attendu : l�ve l'exception OutOfRarray
		@Test
		public void testReplaceIndNP() {
			try{
				A.replace(6, d);
				fail();
			} catch(OutOfRarray e){

			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertFalse(A.contains(d));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
			assertEquals(0,A.nbOcc(d));
		}
		
		// objectif : remplace un objet � un autre � indice non pr�sent
		// resultat attendu : l�ve l'exception OutOfRarray
		@Test
		public void testReplaceIndNPHA() {
			try{
				A.replace(11, d);
				fail();
			} catch(OutOfRarray e){

			}
			assertEquals(6,A.size());
			assertTrue(A.contains(a));
			assertTrue(A.contains(b));
			assertTrue(A.contains(c));
			assertFalse(A.contains(d));
			assertEquals(3,A.nbOcc(a));
			assertEquals(1,A.nbOcc(b));
			assertEquals(2,A.nbOcc(c));
			assertEquals(0,A.nbOcc(d));
		}
}

	

