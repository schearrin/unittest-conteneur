package test;

import testEtat.*;
import junit.framework.*;

public class TestPlein extends TestCase {

	private Conteneur C;
	private Object K1, K2, K3, K4, K5, V1, V2, V3, V4, V5;
	private int CAPACITE = 5;

	// Creation d'un conteneur plein
	public void setUp() {
		try  {
			C = new Conteneur(CAPACITE);
			K1 = new Object();
			K2 = new Object();
			K3 = new Object();
			K4 = new Object();
			K5 = new Object();
			V1 = new Object();
			V2 = new Object();
			V3 = new Object();
			V4 = new Object();
			V5 = new Object();
			C.ajouter(K1, V1);
			C.ajouter(K2, V2);
			C.ajouter(K3, V3);
			C.ajouter(K4, V4);
			C.ajouter(K5, V5);
		} catch (Exception e) {
			fail();
		}
	}
	
    protected void tearDown() throws Exception {
        super.tearDown();

        C = null;
    }

    
    
	// Objectif de test : test taille
	// Resultat attendu : vrai, la taille du conteneur qui est 5
	public void testTaille(){
		assertEquals(C.taille(), 5);
	}
	
	// Objectif de test : test capacite
	// Resultat attendu : vrai, la capacite du conteneur qui est 5
	public void testCapacite(){
		assertEquals(C.capacite(), CAPACITE);
	}
	
	// Objectif de test : test est vide dans un conteneur plein
	// Resultat attendu : test non vide, erreur si levee exception
	public void testEstVidePlein(){
		try{			
			assertFalse(C.estVide());
			
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}
	}

	// Objectif de test : tester si une clef est presente
	// Resultat attendu : erreur si levee exception
	public void testPresentPlein(){
		try{
			assertTrue(C.present(K3));
			
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}
	}

	// Objectif de test : tester si une clef est pas presente
	// Resultat attendu : erreur si levee exception
	public void testPasPresentPlein(){
		try{
			Object K = new Object();
			assertFalse(C.present(K));
			
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}
	}

	
	
	// Objectif de test : retrouver la valeur associee a une clef presente
	// Resultat attendu : possible, erreur si levee exception
	public void testValeurClefPresentePlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			
			assertTrue(C.present(K4));
			C.valeur(K4);
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : retrouver la valeur associee a une clef absente
	// Resultat attendu : impossible, exception erreur conteneur signalee
	public void testValeurClefAbsentePlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();
			
			C.valeur(K);
			
			fail("fail, impossible de trouver la valeur");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail("fail, si levee d'une autre exception que erreur conteneur");
		}
	}
	
	// Objectif de test : associee une meme valeur existante a deux clefs distinctes
	// Resultat attendu : possible
	public void testAssocieValeurA2ClefPlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			assertEquals(C.valeur(K1),V1);
			assertEquals(C.valeur(K2),V2);
			
			C.ajouter(K2, V1);
			
			assertEquals(C.valeur(K1),V1);
			assertEquals(C.valeur(K2),V1);
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail();
		}
	}
	
	// Objectif de test : associee une meme valeur non existante a deux clefs distinctes
	// Resultat attendu : possible
	public void testAssocieValeurA2ClefPlein2(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			assertTrue(C.present(K1));
			assertTrue(C.present(K2));
			assertEquals(C.valeur(K1),V1);
			assertEquals(C.valeur(K2),V2);
			Object V = new Object();
			
			C.ajouter(K1, V);
			C.ajouter(K2, V);
			
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			assertEquals(C.valeur(K1),V);
			assertEquals(C.valeur(K2),V);
		}
		catch(ErreurConteneur e){
			fail();
		}
	}
	
	
	
	// Objectif de test : ajout d'un element dont la cle est deja presente dans un conteneur plein
	// Resultat attendu : ajout possible et ancien couple de meme cle ecrase
	public void testAjouterPresentPlein() {
		try {
			// on verifie qu'apres initialisation, A2 est bien present et que la valeur associee est bien B2
			assertTrue(C.present(K2));
			assertEquals(C.valeur(K2),V2);
			
			Object V = new Object();
			C.ajouter(K2, V);

			// on verifie que l'ancien couple a bien ete ecrase par le nouveau
			assertTrue(C.present(K2));
			assertEquals(C.valeur(K2), V);
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), 5);
		}
		catch (ErreurConteneur e) {
			fail();
		}
	}
	
	// Objectif de test : ajout d'un nouveau couple clef-valeur a un conteneur plein
	// Resultat attendu : exception debordement conteneur signalee
	public void testAjouterNonPresentPlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			
			Object K = new Object();
			Object V = new Object();
			
			C.ajouter(K, V);
			assertTrue(C.present(K));
			assertEquals(C.valeur(K),V);
			fail("fail, le conteneur est deja plein");
			
		}
		catch(DebordementConteneur e){
			assertTrue(C.present(K1));
			assertTrue(C.present(K2));
			assertTrue(C.present(K3));
			assertTrue(C.present(K4));
			assertTrue(C.present(K5));
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail("fail, si autre type d'erreur conteneur que celui du debordement conteneur");
		}
	}
	
	
	
	// Objectif de test : retirer un couple clef-valeur dont la cle est presente
	// Resultat attendu : possible
	public void testRetirerPresentPlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			assertTrue(C.present(K1));
			assertEquals(C.valeur(K1),V1);
			
			C.retirer(K1);
			
			assertFalse(C.present(K1));
			assertEquals(C.taille(), 4);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail();
		}
	}
	
	// Objectif de test : retirer une clef absente d'un conteneur
	// Resultat attendu : rien ne se passe donc pas de levee d'exception
	public void testRetirerAbsentPlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();

			C.retirer(K);
			
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}		
	}
	
	
	
	// Objectif de test : remise a zero d'un conteneur plein
	// Resultat attendu : remise a zero possible, sans changer sa capacite
	public void testRazPlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);

			C.raz();

			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail();
		}
		catch(Exception e){
			fail();
		}
	}
	
	
	
	// Objectif de test : redimensionner un conteneur plein en fixant une capacite plus grande
	// Resultat attendu : possible pour un conteneur plein
	public void testRedimPlusGrandePlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);

			C.redimensionner(CAPACITE*2);

			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE*2);
		}
		catch(ErreurConteneur e){
			fail();
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : redimensionner un conteneur plein en fixant une capacite plus grande
	// Resultat attendu : possible pour un conteneur plein
	public void testRedimPlusGrande2Plein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);

			C.redimensionner(CAPACITE*1000);

			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE*1000);
		}
		catch(ErreurConteneur e){
			fail();
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : redimensionner un conteneur plein en fixant une capacite plus grande
	// Resultat attendu : possible pour un conteneur plein
	public void testRedimPlusGrande3Plein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);

			C.redimensionner(CAPACITE+1);

			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE+1);
		}
		catch(ErreurConteneur e){
			fail();
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : redimensionner un conteneur plein en fixant une capacite plus petite
	// Resultat attendu : impossible, levee de l'exception erreur conteneur
	public void testRedimPlusPetitePlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			
			C.redimensionner(3);
			
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), 3);
			fail("fail, impossible de redimensionner");
		}
		catch(ErreurConteneur e){
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail("fail, si levee d'une autre exception que erreur conteneur");
		}
	}

	// Objectif de test : redimensionner un conteneur plein en fixant une capacite egale
	// Resultat attendu : impossible, levee de l'exception erreur conteneur
	public void testRedimEgalPlein(){
		try{
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			
			C.redimensionner(CAPACITE);
			
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
			fail("fail, impossible de redimensionner");
		}
		catch(ErreurConteneur e){
			assertEquals(C.taille(), 5);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail("fail, si levee d'une autre exception que erreur conteneur");
		}
	}
	
	
}

