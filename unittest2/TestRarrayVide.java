package test;
import rarray.*;

import org.junit.*;

import static org.junit.Assert.*;

public class TestRarrayVide {

	Rarray A;
	Object a;

	@Before
	public void creerRarrayVide() throws Exception {

		try {
			A = new Rarray(10);
			a = new Object();
		} catch (RarrayError e) {
			fail();
		}
	}
	
	// objectif : test preambule d'un Rarray vide
	// resultat attendu : le Rarray est bien vide
	@Test
	public void testPreambuleVide(){
		assertNotNull(A);
		assertEquals(0, A.size());
		assertFalse(A.contains(a));
	}
	
	// objectif : tester l'ajout d'un element dans un Rarray vide
	// resultat attendu : l'element est ajoute en un exemplaire
	@Test
	public void testAjouterVide() {
		A.add(a);

		assertTrue(A.contains(a));
		assertEquals(1,A.nbOcc(a));
		assertEquals(1,A.size());
	}
	
	// objectif : tester la suppression d'un element dans un Rarray vide
	// resultat attendu : on ne peut pas supprimer un element si c'est vide
	@Test
	public void testSupprimerVide(){
		A.remove(a);
		
		assertFalse(A.contains(a));
		assertEquals(0,A.nbOcc(a));
		assertEquals(0,A.size());
		
	}
	
	// objectif : tester la remise a zero d'un Rarray vide
	// resultat attendu : le Rarray reste vide
	@Test
	public void testRAZVide(){		
		A.clear();
		
		assertFalse(A.contains(a));
		assertEquals(0, A.size());
		assertEquals(0,A.nbOcc(a));
		assertEquals(0,A.size());
	}
	
	//
	
	// objectif : test recherche l'objet present � un indice dans un Rarray vide
	// resultat attendu : leve l'exception OutOfRarray
	@Test
	public void testGet() {
		try{
			A.get(0);
			fail();
		} catch(OutOfRarray e){

		}
		assertNotNull(A);
		assertEquals(0, A.size());
		assertFalse(A.contains(a));
	}
	
	// objectif : test si l'objet dont on cherche l'indice est present
	// resultat attendu : indice non present, leve l'exception ObjectNotFound
	@Test
	public void testIndexNotExist() {
		try{
			A.index(a);
			fail();
		} catch(ObjectNotFound e){

		}
		assertNotNull(A);
		assertEquals(0, A.size());
		assertFalse(A.contains(a));
	}
	
	// objectif : supprime l'objet d'un indice dans un Rarray vide
	// resultat attendu : leve l'exception OutOfRarray
		@Test
		public void testRemoveInd() {
			try{
				A.removeInd(0);
				fail();
			} catch(OutOfRarray e){

			}
			assertNotNull(A);
			assertEquals(0, A.size());
			assertFalse(A.contains(a));
	}
	
	// objectif : remplace un objet � un autre � indice pr�sent
	// resultat attendu : fonctionne
		@Test
		public void testReplace() {
			try{
				A.replace(0, a);
				fail();
			} catch(OutOfRarray e){

			}
			assertNotNull(A);
			assertEquals(0, A.size());
			assertFalse(A.contains(a));

		}

	
}
