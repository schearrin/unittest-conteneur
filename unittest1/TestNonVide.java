package test;

import testEtat.*;
import junit.framework.*;

public class TestNonVide extends TestCase {

	private Conteneur C;
	private Object K1, K2, B1, V2;
	private int CAPACITE = 5;

	// Creation d'un conteneur partiellement rempli
	public void setUp() {
		try {
			C = new Conteneur(CAPACITE);
        	K1 = new Object();
        	K2 = new Object();
        	B1 = new Object();
        	V2 = new Object();
        	C.ajouter(K1, B1);
        	C.ajouter(K2, V2);
		} catch (Exception e) {
			fail();
		}
	}
	
	
	// Objectif de test : test taille
	// Resultat attendu : vrai, la taille du conteneur qui est 2
	public void testTaille(){
		assertEquals(C.taille(), 2);
	}
	
	// Objectif de test : test capacite
	// Resultat attendu : vrai, la capacite du conteneur qui est 5
	public void testCapacite(){
		assertEquals(C.capacite(), CAPACITE);
	}
	
	// Objectif de test : test est vide dans un conteneur non vide
	// Resultat attendu : test non vide, erreur si levee exception
	public void testEstVideNonVide(){
		try{			
			assertFalse(C.estVide());
			
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : tester si une clef est presente
	// Resultat attendu : erreur si levee exception
	public void testPresentPlein(){

		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			
			assertTrue(C.present(K2));
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : tester si une clef est pas presente
	// Resultat attendu : erreur si levee exception
	public void testPasPresentPlein(){
		try{
			Object K = new Object();
			assertFalse(C.present(K));
			
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(Exception e){
			fail();
		}
	}


	
	// Objectif de test : retrouver la valeur associee a une clef presente
	// Resultat attendu : possible, erreur si levee exception
	public void testValeurClefPresenteNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			
			assertTrue(C.present(K2));
			C.valeur(K2);
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : retrouver la valeur associee a une clef absente
	// Resultat attendu : impossible, exception erreur conteneur signalee
	public void testValeurClefAbsenteNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();
			
			C.valeur(K);
			
			fail("fail, impossible de trouver la valeur");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail("fail, si levee d'une autre exception que erreur conteneur");
		}
	}

	// Objectif de test : associee une meme valeur existante a deux clefs distinctes
	// Resultat attendu : possible
	public void testAssocieValeurA2Clef(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			assertEquals(C.valeur(K1),B1);
			assertEquals(C.valeur(K2),V2);
			
			C.ajouter(K2, B1);
			
			assertEquals(C.valeur(K1),B1);
			assertEquals(C.valeur(K2),B1);
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail();
		}
	}
	
	// Objectif de test : associee une meme valeur non existante a deux clefs distinctes
	// Resultat attendu : possible
	public void testAssocieValeurA2Clef2(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			assertTrue(C.present(K1));
			assertTrue(C.present(K2));
			assertEquals(C.valeur(K1),B1);
			assertEquals(C.valeur(K2),V2);
			Object V = new Object();
			
			C.ajouter(K1, V);
			C.ajouter(K2, V);
			
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			assertEquals(C.valeur(K1),V);
			assertEquals(C.valeur(K2),V);
		}
		catch(ErreurConteneur e){
			fail();
		}
	}
	
	// Objectif de test : associee une meme valeur a deux clefs distinctes non existante
	// Resultat attendu : possible
	public void testAssocieValeurA2Clef3(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			Object K10 = new Object();
			Object K11 = new Object();
			Object V = new Object();
			
			C.ajouter(K10, V);
			C.ajouter(K11, V);
			
			assertEquals(C.taille(), 4);
			assertEquals(C.capacite(), CAPACITE);
			assertEquals(C.valeur(K10),V);
			assertEquals(C.valeur(K11),V);
		}
		catch(Exception e){
			fail();
		}
	}
	
	
	
	// Objectif de test : ajout d'un element dont la cle est deja presente dans un conteneur non vide
	// Resultat attendu : possible et ancien couple de meme cle ecrase
	public void testAjouterPresentNonVide() {
		try {
			assertTrue(C.present(K2));
			assertEquals(C.valeur(K2),V2);
			
			Object V = new Object();
			C.ajouter(K2, V);

			assertTrue(C.present(K2));
			assertEquals(C.valeur(K2), V);
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch (ErreurConteneur e) {
			fail();
		}
	}
	
	// Objectif de test : ajout d'un nouveau couple clef-valeur a un conteneur non vide
	// Resultat attendu : possible, erreur si levee exception
	public void testAjouterNonPresentNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();
			Object V = new Object();
			
			C.ajouter(K, V);
			
			assertTrue(C.present(K));
			assertEquals(C.valeur(K),V);
			assertEquals(C.taille(), 3);
			assertEquals(C.capacite(), CAPACITE);
			
		}
		catch(ErreurConteneur e){
			fail();
		}
		catch(Exception e){
			fail();
		}
	}
	
	// Objectif de test : ajout d'un nouveau couple clef-valeur avec une clef null
	// Resultat attendu : impossible, levee exception erreur conteneur
	public void testAjouterClefNull(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			
			Object K = null;
			Object V = new Object();
			C.ajouter(K, V);
			
			assertEquals(C.taille(), 3);
			assertEquals(C.capacite(), CAPACITE);
			fail("fail, impossible d'ajouter une clef null");
			
		}
		catch(DebordementConteneur e){
			fail();
		}
		catch(ErreurConteneur e){
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
		}
	}
	
	// Objectif de test : ajout d'un nouveau couple clef-valeur avec une valeur null
	// Resultat attendu : impossible, levee exception erreur conteneur
	public void testAjouterValeurNull(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			
			Object K = new Object();
			Object V = null;
			C.ajouter(K, V);
			
			assertEquals(C.taille(), 3);
			assertEquals(C.capacite(), CAPACITE);
			fail("fail, impossible d'ajouter une valeur null");
		}
		catch(DebordementConteneur e){
			fail();
		}
		catch(ErreurConteneur e){
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
		}
	}

	
	
	// Objectif de test : retirer un couple clef-valeur dont la cle est presente
	// Resultat attendu : possible
	public void testRetirerPresentNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			assertTrue(C.present(K1));
			assertEquals(C.valeur(K1),B1);
			
			C.retirer(K1);
			
			assertFalse(C.present(K1));
			assertEquals(C.taille(), 1);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail();
		}
	}
	
	// Objectif de test : retirer une clef absente d'un conteneur
	// Resultat attendu : rien ne se passe donc pas de levee d'exception
	public void testRetirerAbsentNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			Object K = new Object();

			C.retirer(K);	
		}
		catch(Exception e){
			fail();
		}		
	}
	
	
	
	// Objectif de test : remise a zero d'un conteneur non vide
	// Resultat attendu : remise a zero possible, sans changer sa capacite
	public void testRazNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);

			C.raz();

			assertEquals(C.taille(), 0);
			assertEquals(C.capacite(), CAPACITE);
		}
		catch(ErreurConteneur e){
			fail();
		}
		catch(Exception e){
			fail();
		}
	}
	
	
	
	// Objectif de test : redimensionner un conteneur non vide en fixant une capacite plus grande
	// Resultat attendu : impossible si le conteneur non plein, levee exception erreur conteneur
	public void testRedimPlusGrandeNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);

			C.redimensionner(10);

			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), 10);
			fail("fail, impossible de redimensionner, le conteneur n'est pas encore plein");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail("fail, si levee d'une exception autre que erreur conteneur");
		}
	}
	
	// Objectif de test : redimensionner un conteneur non vide en fixant une capacite plus petite
	// Resultat attendu : impossible, levee de l'exception erreur conteneur
	public void testRedimPlusPetiteNonVide(){
		try{
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), CAPACITE);
			
			C.redimensionner(3);
			
			assertEquals(C.taille(), 2);
			assertEquals(C.capacite(), 3);
			fail("fail, impossible de redimensionner");
		}
		catch(ErreurConteneur e){
			
		}
		catch(Exception e){
			fail();
		}
	}

}
